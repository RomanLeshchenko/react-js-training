import React from 'react';
import {Link} from 'react-router-dom';

const Header = () => {
    return (
        <div>
            <p><Link to="/users">Users</Link></p>
            <p><Link to="/courses">Courses</Link></p>
        </div>
    )
}

export default Header;