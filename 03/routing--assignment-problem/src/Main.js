import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import Courses from './containers/Courses/Courses';
import Users from './containers/Users/Users';
import HomePage from './HomePage';
import NotFound from './NotFound';

const Main = () => {
    return (
        <div>
            <Switch>
                <Route exact path="/" component={HomePage}/>
                <Route path="/users" component={Users}/>
                <Route path="/courses" component={Courses} />
                <Route exact path="/all-courses" render={() => (<Redirect to="/courses" />)} />
                <Route component={NotFound} />
            </Switch>
        </div>
    )
}

export default Main;