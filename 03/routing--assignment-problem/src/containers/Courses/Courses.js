import React, { Component } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";

import FullCourses from './FullCourses';
import Course from '../Course/Course';

const Courses = (props) => (
    <Switch>
        <Route exact path="/courses" component={FullCourses}/>
        <Route exact path="/courses/:id" component={Course}/>
    </Switch>
)

export default Courses;