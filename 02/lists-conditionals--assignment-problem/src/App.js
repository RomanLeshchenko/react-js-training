import React, { Component } from 'react';
import './App.css';
import ValidationComponent from './components/ValidationComponent';
import CharComponent from './components/CharComponent';

class App extends Component {

  state = {
    inputText : ""
  }

  inputTextChangedHandler = (e) => {
    this.setState({
      inputText: e.target.value
    })
  }

  removeCharacterFromInput = (e, index) => {
      const currentTextArray = this.state.inputText.split("");
      currentTextArray.splice(index, 1);

      this.setState({
        inputText : currentTextArray.join("")
      })
  }

  render() {
    let chars = (
       <div>
        { this.state.inputText.split("").map((c, index) => {
          return <CharComponent character={c}
                                key={index}
                                click={(event) => this.removeCharacterFromInput(event, index)}/>
        })}
       </div>
    );


    return (
      <div className="App">
        <input type="text"
               placeholder="Enter Your Text"
               value={this.state.inputText}
               onChange={(event) => this.inputTextChangedHandler(event)} />

        <p>Length is: {this.state.inputText.length}</p>
        {chars}
        <ValidationComponent inputTextLength={this.state.inputText.length}/>
      </div>
    );
  }
}

export default App;
