import React from 'react';

const ValidationComponent = (props) => {
    const style = {
        fontSize: "18px",
        color:"red"
    }

    const inputTextLength = props.inputTextLength;
    const minLength = 5;

    if (inputTextLength < minLength) {
        return (<div style={style}>Text too short</div>)
    }

    else {
        return (<div></div>)
    }
}

export default ValidationComponent;