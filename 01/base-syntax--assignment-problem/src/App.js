import React, { Component } from 'react';
import './App.css';
import './components/UserInput';
import './components/UserOutput';
import UserInput from './components/UserInput';
import UserOutput from './components/UserOutput';

class App extends Component {

  state = {
    userName : "Roman"
  }

  nameChangedHandler = (event) => {
     this.setState({
      userName : event.target.value
     })
  }

  render() {
    return (
      <div className="App">
        <UserInput changed={this.nameChangedHandler} userName={this.state.userName}/>
        <UserOutput userName={this.state.userName}/>
      </div>
    );
  }
}

export default App;
