import React from 'react';

const UserOutput = (props) => {

    const style = {
        backgroundColor: "green",
        fontSize: "18px",
        textAlign: "center"
      }

    return (
        <div style={style}>
            <p>First paragraph. This is my first react app.</p>
            <p>Second paragraph. My name is {props.userName}.</p>
        </div>
    )
}

export default UserOutput;