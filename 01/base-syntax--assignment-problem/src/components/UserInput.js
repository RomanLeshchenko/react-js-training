import React from 'react';
import '../styles/userInput.css';

const UserInput = (props) => {
    return (
        <div className="userForm">
            <input type="text" onChange={props.changed} value={props.userName}/>
        </div>
    )
}

export default UserInput;